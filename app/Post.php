<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'content', 'fromCoorLat', 'fromCoorLan', 'toCoorLat','toCoorLan','is_accepted'
    ];
}
