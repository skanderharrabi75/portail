import React, { useEffect } from "react";
import axiosInstance from "../../../config/axios-instance";
import { Form,Input } from 'antd';
import {
    Button,
    Label,
    FormGroup,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Container,
    Row,
    Col
} from "reactstrap";



const PostF = (props) => {
    const { getFieldDecorator } = props.form;
    useEffect(() => {
    }, []);
    const handleSubmit = (event,values) => {
        event.preventDefault();
        props.form.validateFields((err, data) => {
            if (!err) {
                console.log(data);
            }else {
                console.log(err);
            }
        });
    };
    return (
        <Container className="posts-page">
            <Form onSubmit={handleSubmit}  className="login-form">
                <Row className="row">
                    <Col xs="6">
                        <Form.Item label="Your Location">
                            {getFieldDecorator('location', {
                                rules: [{ required: true, message: 'Enter your location please!' }],
                                initialValue: props.fromPosition} )(
                                <Input
                                    name="location"
                                    placeholder="Your location"
                                    className="form-control"
                                />,
                            )}
                        </Form.Item>
                    </Col>
                    <Col xs="6">
                    <Form.Item label="Destination">
                        {getFieldDecorator('destination', {
                            rules: [{ required: true, message: 'Enter your destination please!' }],
                            initialValue: props.toPosition} )(
                            <Input
                                placeholder="Your destination"
                                className="form-control"
                            />,
                        )}
                    </Form.Item>
                    </Col>
                </Row>
                <Row className="row">
                    <Col xs="6">
                    <Form.Item label="Content" >
                        {getFieldDecorator('content', {
                            rules: [{required: true, message: 'You Need to enter what you need !'}],
                            initialValue: props.toPosition} )(
                            <Input
                                placeholder="Content"
                                className="form-control"
                            />,
                        )}
                    </Form.Item>
                    </Col>
                </Row>
                <Row>
                    <>
                        <Form.Item>
                            {getFieldDecorator('fromCoor', {
                                initialValue: props.fromCoor,
                            } )(
                            <Input
                                className="has-error"
                                disabled
                                hidden
                            />,
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('toCoor', {
                                initialValue: props.toCoor} )(
                                <Input
                                    className="has-error"
                                    disabled
                                    hidden
                                />,
                            )}
                        </Form.Item>
                    </>
                </Row>
                <Row className="row">
                    <Col xs="4"></Col>
                    <Col xs="8">
                        <Form.Item>
                            <Button type="submit"> Post </Button>
                        </Form.Item>
                    </Col>
                    <Col xs="4"></Col>
                </Row>
            </Form>
        </Container>
    );
};

const PostForm = Form.create({ name: 'contact_form' })(PostF);
export default PostForm;
