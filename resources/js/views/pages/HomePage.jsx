/*!

=========================================================
* BLK Design System React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/blk-design-system-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/blk-design-system-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, {useEffect} from "react";

// core components
import  ExamplesNavbar from "../../components/Navbars/ExamplesNavbar.js";
import PageHeader from "../../components/PageHeader/PageHeader.jsx";
import Footer from "../../components/Footer/Footer.js";

// sections for this page/view
import Basics from "../sections/Basics.jsx";
import Navbars from "../sections/Navbars.jsx";
import Tabs from "../sections/Tabs.jsx";
import Pagination from "../sections/Pagination.jsx";
import Notifications from "../sections/Notifications.jsx";
import Typography from "../sections/Typography.jsx";
import JavaScript from "../sections/JavaScript.jsx";
import NucleoIcons from "../sections/NucleoIcons.jsx";
import Signup from "../sections/Signup.jsx";
import Examples from "../sections/Examples.jsx";
import Download from "../sections/Download.jsx";
import MapJibFidk from "../sections/Map-section/MapJibFidk";

const HomePage =prop => {
    useEffect(() => {
        document.body.classList.toggle("index-page");
        return () => {
            document.body.classList.toggle("index-page");
        }
    }, []);
  return (
      <>
        <ExamplesNavbar />
        <div className="wrapper">
          <PageHeader />
          <div className="main">
            <MapJibFidk/>
            <Basics />
            {/*<Navbars />*/}
            {/*<Tabs />*/}
            {/*<Pagination />*/}
            {/*<Notifications />*/}
            {/*<Typography />*/}
            {/*<JavaScript />*/}
            {/*<NucleoIcons />*/}
            {/*<Signup />*/}
            {/*<Examples />*/}
            {/*<Download />*/}
          </div>
          <Footer />
        </div>
      </>
    );
};

export default HomePage;
